import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()	
#Defining the Available functions
def site_login():
	driver.get ("https://twitter.com/login?lang=en-gb")
	driver.find_element_by_name("session[username_or_email]").send_keys("EMAIL")
	driver.find_element_by_name("session[password]").send_keys("Password")
	driver.find_element_by_class_name("submit EdgeButton EdgeButton--primary EdgeButtom--medium").click()
	print("Site_Login Complete")

def Post_Tweet():
    driver.get ("https://twitter.com/")
    driver.find_element_by_class_name("text").click()
	driver.find_element_by_class_name("tweet-box rich-editor").send_keys(TWEET)
	driver.find_element_by_class_name("button-text").click()
	print("Post_Tweet Complete")

def Follow_User():
    driver.get (USER_URL)
	driver.find_element_by_id("user-actions-follow-button js-follow-btn follow-button").click()
	
def Send_DM():
	driver.get ("https://twitter.com/")
	driver.find_element_by_class_name("js-tooltip js-dynamic-tooltip global-dm-nav").click()
	driver.find_element_by_class_name("DMInbox-toolbar DMComposeButton EdgeButton EdgeButton--small EdgeButton--primary dm-new-button js-initial-focus").click()
	driver.find_element_by_class_name("TokenizedMultiselect-input twttr-directmessage-input js-initial-focus dm-to-input").send_keys(RECIPIENT)
	driver.find_element_by_class_name("EdgeButton EdgeButton--primary dm-initiate-conversation").click()
	driver.find_element_by_class_name("DMComposer-editor tweet-box rich-editor js-initial-focus is-showPlaceholder").send_keys(DM)
	print("Send_DM Complete")

#Call Functions Here
#site_login()
#Post_Tweet()
#Follow_User()
#Send_DM()
